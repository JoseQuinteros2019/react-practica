

import './App.css';
import Titulo from './components/Titulo';
import ListName from './components/ListName';



import React, { Component } from 'react';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTask: "",
      tasks:[]
    }
  }
  handleNewTask = (event) => {
    console.log(event.target.value);
    this.setState({newTask:event.target.value})// se usa para guardar datos en el State 
  }
  handleSaveTask = (event) => {
    event.preventDefault();// evita que recargue la pagina
    let _newTask = this.state.tasks;
    _newTask.push(this.state.newTask);
    this.setState({
      tasks: _newTask,
      newTask:"",
    })
    
  }
  render() {
    return (
      <div>
        <Titulo name="Nico" edad="24 años"/>
        <ListName />
        <form onSubmit={this.handleSaveTask}>  
        <input type="text" className="new-task" onChange={this.handleNewTask} value={this.state.newTask}></input>
       </form>
        
      </div>
    );
  }
}



export default App;