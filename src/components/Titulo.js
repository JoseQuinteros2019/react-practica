import React from 'react';

const Titulo = (props) => {
    return (
        <div>
            <h1 className="App-link">  Bienvenido {props.name} de {props.edad} </h1>
        </div>
    );
};

export default Titulo;